//console.log("Hola node");
var express = require('express');
var userFile = require('./user.json');
var fs = require('fs');
var jsonDataBaseUsers = '{"data" : ' + JSON.stringify(userFile) + '}';
var bodyParser = require('body-parser');

var app = express();
app.use(bodyParser.json());
var totalUsers = 0;

const SERVICE_PATH_BASE = '/apitechu/v1/';
const PORT = process.env.PORT || 3000;

//Peticion GET de todos los 'users' (Collections)
app.get(SERVICE_PATH_BASE + 'users',
  //app.get('/',
  function(req, res){
    console.log('GET ' + SERVICE_PATH_BASE + 'users');
    res.send(jsonDataBaseUsers);
});

//Peticion GET de un 'user' (Instance)
app.get(SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
    console.log('GET ' + SERVICE_PATH_BASE + 'users/id');
    let indice = req.params.id;
    let instancia = userFile[indice - 1]
    let respuesta = instancia != undefined ? instancia : {"mensaje":"recurso no encontrado"};
    res.status(200);
    res.send(respuesta);
  }
);

//Peticion GET de varios 'user' (Collection) segun query
app.get(SERVICE_PATH_BASE + 'usersq',
  function(req, res){
    console.log('GET ' + SERVICE_PATH_BASE + 'con query string');
    let queryLoggedCondition = (req.query.logged == "false") ? 0 : ((req.query.logged == "true") ? 1 : 2);
    let beginIndex = req.query.beginIndex > 0 ? req.query.beginIndex : 1;
    let size = req.query.size > 0 ? req.query.size : userFile.length - beginIndex + 1;
    let count = 0;
    let i = 0;
    let users = userFile;
    let returnUsers = [];
    for (i = beginIndex - 1; count < size && i < userFile.length; i++) {
      let u = userFile[i];
      let userLoggedCondition = (u.logged == true) ? 1 : 0;
      if (queryLoggedCondition == 2 || queryLoggedCondition == userLoggedCondition) {
        returnUsers.push(u);
        count++;
      }
    }
    res.status(200);
    res.send(returnUsers);
  }
);

//Peticion POST para crear un usuario nuevo
app.post(SERVICE_PATH_BASE + 'users',
  function(req, res){
      totalUsers = userFile.length;
      let isValid = (
            req.body.first_name   != null && req.body.first_name  != "" &&
            req.body.last_name    != null && req.body.last_name   != "" &&
            req.body.email        != null && req.body.email       != "" &&
            req.body.password     != null && req.body.password    != ""
    );
    if(isValid) {
      let newUser = {
        userID : totalUsers,
        first_name : req.body.first_name,
        last_name : req.body.last_name,
        email : req.body.email,
        password : req.body.password
      }
      userFile.push(newUser);
      res.status(200);
      res.send({"mensaje" : "Usuario creado con éxito.", "usuario" : newUser});
    } else {
      res.status(500);
      res.send({"mensaje" : "Ingrese todos los datos de usuario."});
    }

  }
);

//Peticion PUT para modificar los datos de un usuario
app.put(SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
      //totalUsers = userFile.length;
      let indice = req.params.id;
      let isValid = (
        (req.body.first_name   != null && req.body.first_name  != "") ||
        (req.body.last_name    != null && req.body.last_name   != "") ||
        (req.body.email        != null && req.body.email       != "") ||
        (req.body.password     != null && req.body.password    != "")
    ) && (indice < userFile.length);
    if(isValid) {

      console.log('PUT ' + SERVICE_PATH_BASE + 'users');

      let instancia = userFile[indice - 1];
      let respuesta = instancia != undefined ? instancia : {"mensaje":"recurso no encontrado"};
      instancia.first_name = req.body.first_name,
      instancia.last_name = req.body.last_name,
      instancia.email = req.body.email,
      instancia.password = req.body.password
      userFile[indice - 1] = instancia;
      res.status(201);
      res.send({"mensaje" : "Usuario modificado con éxito.", "usuario" : instancia});
    } else {
      res.status(500);
      res.send({"mensaje" : "Error al actualizar usuario."});
    }

  }
);

//Peticion DELETE de un 'user' (Instance)
//Hacerlo en el body cuando sea un dato sensible
app.delete(SERVICE_PATH_BASE + 'users/:id',
  function(req, res){
    console.log('DELETE ' + SERVICE_PATH_BASE + 'users/id');
    let indice = req.params.id;
    let instanciaEliminar = userFile[indice - 1];
    instanciaEliminar = userFile.splice(indice - 1,1);
    let respuesta = instanciaEliminar != undefined ? instanciaEliminar : {"mensaje":"recurso no encontrado"};
    res.status(204);
    res.send(respuesta);
  }
);

//Login de usuario
app.post(SERVICE_PATH_BASE + 'login',
  function(req, res){
    console.log('POST ' + SERVICE_PATH_BASE + 'login');
    let logged = false;
    let isValid = (
        (req.body.email        != null && req.body.email       != "") &&
        (req.body.password     != null && req.body.password    != "")
    );
    var users = userFile;
    if(isValid) {
      for (u of users) {
        if (req.body.email == u.email && req.body.password == u.password) {
          u.logged = true;
          logged = true;
          break;
        }
      }

      if (logged) {
        writeUserDataToFile(users);
        res.status(200);
        res.send({"mensaje" : "Sesión iniciada correctamente."});
      } else {
        res.status(400);
        res.send({"mensaje" : "Ocurrió un error de acceso, el usuario o clave no son correctos."});
      }

    } else {
      res.status(500);
      res.send({"mensaje" : "Error validar usuario."});
    }

  }
);

//Logout de usuario
app.post(SERVICE_PATH_BASE + 'logout',
  function(req, res){
    console.log('POST ' + SERVICE_PATH_BASE + 'logout');
    let logged = false;
    let isValid = (
        (req.body.email        != null && req.body.email       != "")
    );
    var users = userFile;
    if(isValid) {
      for (u of users) {
        if (req.body.email == u.email && u.logged != null && u.logged == true) {
          delete u.logged;
          logged = true;
          break;
        }
      }

      if (logged) {
        writeUserDataToFile(users);
        res.status(200);
        res.send({"mensaje" : "Sesión finalizada correctamente."});
      } else {
        res.status(400);
        res.send({"mensaje" : "Usuario no logeado."});
      }
    } else {
      res.status(500);
      res.send({"mensaje" : "Error al validar usuario."});
    }

  }
);

function writeUserDataToFile(data) {
   var jsonUserData = JSON.stringify(data);
   fs.writeFile("./user.json", jsonUserData, "utf8",
    function(err) { //función manejadora para gestionar errores de escritura
      if(err) {
        console.log(err);
      } else {
        console.log("Datos escritos en 'users.json'.");
      }
    })
 }

app.listen(PORT, function() {
  console.log('API escuchando en puerto 3000...');
});
